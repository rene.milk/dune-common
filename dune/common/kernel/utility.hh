// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_COMMON_KERNEL_UTILITY_HH
#define DUNE_COMMON_KERNEL_UTILITY_HH

#include <dune/common/typetraits.hh>
#include <dune/common/memory/alignment.hh>

#define DUNE_RESTRICT __restrict__
#define DUNE_NOINLINE __attribute__((noinline))

#endif // DUNE_COMMON_KERNEL_UTILITY_HH
