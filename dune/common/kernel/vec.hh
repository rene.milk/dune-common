// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_COMMON_KERNEL_VEC_HH
#define DUNE_COMMON_KERNEL_VEC_HH

#include <dune/common/kernel/vec/add.hh>
#include <dune/common/kernel/vec/assign_scalar.hh>
#include <dune/common/kernel/vec/axpy.hh>
#include <dune/common/kernel/vec/dot.hh>
#include <dune/common/kernel/vec/infinity_norm.hh>
#include <dune/common/kernel/vec/maskeddot.hh>
#include <dune/common/kernel/vec/mul.hh>
#include <dune/common/kernel/vec/one_norm.hh>
#include <dune/common/kernel/vec/scale.hh>
#include <dune/common/kernel/vec/shift.hh>
#include <dune/common/kernel/vec/subtract.hh>
#include <dune/common/kernel/vec/two_norm2.hh>

#endif // DUNE_COMMON_KERNEL_VEC_HH
