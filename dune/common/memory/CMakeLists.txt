#install headers
install(FILES
  alignment.hh
  blocked_allocator.hh
  domain.hh
  traits.hh
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/common/memory)
